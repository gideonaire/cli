<?php
  if (php_sapi_name() != 'cli') {
    printf("Please run from the command line.\n");
    exit(1);
  }

  $opts = [
    "install-dir:",
    "filename:"
  ];

  $args = getopt(null, $opts);

  $installDir = $args["install-dir"] ?? "/usr/local/bin";
  $xanoFile = $args["filename"] ?? "xano";

  printf("Welcome to Xano CLI setup.\n\n");

  $archiveFile = sprintf("%s/xanocli.tar.gz", __DIR__);
  $tmpDir = sprintf("%s/xanocli-%s", __DIR__, System::uniqueId());

  $setup = new Setup($installDir, $xanoFile, $archiveFile, $tmpDir);

  try {
    run("checking php version", [$setup, "checkVersion"]);
    run("checking php extensions", [$setup, "checkExtensions"]);
    run("locating installation directory", [$setup, "locateIncludePath"]);
    run("checking file permissions", [$setup, "checkPermissions"]);
    run("downloading xanocli", [$setup, "downloadXanoCLI"]);
    run("unpacking xanocli", [$setup, "unpackXanoCLI"]);
    run("installing xanocli", [$setup, "installXanoCLI"]);
    run("cleaning up", [$setup, "cleanup"]);
    run("validating installation", [$setup, "validateInstall"]);
  } catch(\Exception $e) {
    printf("Whoops! Something went wrong.\n\n%s\n\n", $e->getMessage());
    $setup->cleanup();
    exit(1);
  }

  function run($msg, $func, $args = []) {
    printf("%s... ", $msg);
    $ret = call_user_func_array($func, $args);
    if ($ret !== FALSE) {
      printf("ok\n");
    }
  }

  class Setup {
    function __construct($installDir, $xanoFile, $archiveFile, $tmpDir) {
      $installDir = System::realpath($installDir);

      $this->fullXanoPath = System::mergePaths($installDir, $xanoFile);
      $this->installDir = $installDir;
      $this->xanoFile = $xanoFile;
      $this->archiveFile = $archiveFile;
      $this->tmpDir = $tmpDir;
    }

    function checkVersion() {
      $v = explode(".", PHP_VERSION);
      if ($v[0] < 7 || $v[1] < 1) {
        throw new \Exception("Please upgrade your PHP to at least 7.1");
      }
    }

    function checkExtensions() {
      foreach([
        "curl",
        "gmp",
        "json",
        "mbstring",
        "pgsql",
        "posix",
        "zlib"
      ] as $ext) {
        if (!extension_loaded($ext)) {
          throw new \Exception("Missing $ext - please install");
        }
      }
    }

    function downloadXanoCLI() {
      System::download(
        "https://gitlab.com/xano/cli/raw/master/dist/xanocli.tar.gz",
        $this->archiveFile
      );
    }

    function unpackXanoCLI() {
      System::mkdir($this->tmpDir);
      $cmd = sprintf("tar xvf %s -C %s", escapeshellarg($this->archiveFile), escapeshellarg($this->tmpDir));
      System::execute($cmd);
    }

    function locateIncludePath() {
      $paths = explode(":", get_include_path());
      foreach($paths as $path) {
        if (strpos($path, "php") !== FALSE) {
          $foundPath = $path;
          break;
        }
      }

      if (!isset($foundPath)) {
        throw new \Exception("Unable to locate appropriate include_path. Please set this value in php.ini configuration file.");
      }

      $this->foundPath = $foundPath;
    }

    function checkPermissions() {
      try {
        $xanoDir = sprintf("%s/xano", $this->foundPath);
        if (!file_exists($xanoDir)) {
          System::mkdir($xanoDir);
        }

        $this->xanoDir = $xanoDir;

        System::assertWritable($xanoDir);

        $to = sprintf("%s/cli", $xanoDir);

        if (file_exists($to)) {
          System::assertWritable($to);
        }

        $testDir = sprintf("%s/xano/cli-test-%s", $this->foundPath, System::uniqueId());
        System::mkdir($testDir);
        System::unlink($testDir);
      } catch(\Exception $e) {
        throw new \Exception("Elevated permissions are required.\nRun with \"sudo\" or make \"$xanoDir\" writable.\n");
      }

      try {
        if (file_exists($this->fullXanoPath)) {
          System::assertWritable($this->fullXanoPath);
        } else {
          $dir = pathinfo($this->fullXanoPath, PATHINFO_DIRNAME);
          if (file_exists($dir)) {
            System::assertWritable($dir);
          } else {
            System::mkdir($dir);
          }
        }
      } catch(\Exception $e) {
        throw new \Exception("Elevated permissions are required.\nRun with \"sudo\" or make \"$this->fullXanoPath\" writable.\n");
      }
    }

    function installXanoCLI() {
      $from = sprintf("%s/xano/cli", $this->tmpDir);
      $to = sprintf("%s/cli", $this->xanoDir);

      if (file_exists($to)) {
        System::unlink($to);
      }

      System::move($from, $to);

      $from = sprintf("%s/index.php", $this->tmpDir);

      System::saveFile($this->fullXanoPath, sprintf("#!%s\n%s", PHP_BINARY, System::readFile($from)));
      System::chmod($this->fullXanoPath, 0755);
    }

    function cleanup() {
      System::unlink($this->archiveFile);
      System::unlink($this->tmpDir);
    }

    function validateInstall() {
      if (!file_exists($this->fullXanoPath)) {
        throw new \Exception("Missing file: $this->fullXanoPath");
      }

      try {
        System::which($this->xanoFile);
        printf("\n\ndone - type \"%s\" to get started.\n", $this->xanoFile);
      } catch(\Exception $e) {
        printf("\n\nNOTICE: The xano-cli was installed here: %s\n", $this->fullXanoPath);
        printf("However, that location is not within your environment PATH.\n");
        printf("If you would not to run the xano-cli by just typing \"%s\", then run the following command:\n\n", $this->xanoFile);
        printf("export PATH=\$PATH:%s\n\n", $this->installDir);
      }
      return FALSE; // skip the "ok" echo
    }
  }

  class System {
    static function readFile($filename) {
      $data = @file_get_contents($filename);
      if ($data === FALSE) {
        throw new \Exception("Unable to read file: $filename");
      }
      return $data;
    }

    static function saveFile($filename, $data, $append = false) {
      $dir = pathinfo($filename, PATHINFO_DIRNAME);
      static::mkdir($dir);

      $ret = @file_put_contents($filename, $data, $append ? FILE_APPEND : 0);
      if ($ret === FALSE) {
        throw new \Exception("Unable to save file - $filename");
      }

      if (strlen($data) !== $ret) {
        @unlink($filename);
        throw new \Exception("Unable to save entire file - $filename. Out of disk space?"); 
      }
    }

    static function copyFile($src, $dst) {
      $dir = pathinfo($dst, PATHINFO_DIRNAME);
      static::mkdir($dir);

      $ret = @copy($src, $dst);
      if (!$ret) {
        throw new \Exception("Unable to copy file: $src to $dst");
      }
    }

    static function mkdir($dir, $mode = 0755) {
      if (!file_exists($dir)) {
        $umask = umask(0);
        $ret = @mkdir($dir, $mode, true);
        umask($umask);

        if (!$ret) {
          throw new \Exception("Unable to create directory: $dir");
        }
      }
    }

    static function chmod($src, $mod) {
      if (@chmod($src, $mod) === FALSE) {
        throw new \Exception("Unable to set permissions on $src\nRun with \"sudo\" to elevate priviledges.");
      }
    }

    static function execute($cmd, $expectedCode = 0) {
      $cmd2 = sprintf("%s 2>&1", $cmd);
      ob_start();
      $ret = 0;
      passthru($cmd2, $ret);
      $data = ob_get_clean();

      if (!is_null($expectedCode) && $ret !== $expectedCode) {
        throw new \Exception("Failed to execute: $cmd\n\nOutput:\n$data");
      }

      return $data;
    }

    static function which($exe) {
      $cmd = sprintf("which %s", escapeshellcmd($exe));
      return static::execute($cmd);
    }

    static function getExecutablePath($exe) {
      try {
        $ret = static::which($exe);
        if (!empty($ret)) {
          if (strpos($ret, "alias") === 0) {
            $ret = explode($ret, "\n");
            unset($ret[0]);
            $ret = trim(implode("\n", $ret));
          } else {
            $ret = trim($ret);
          }
        }

        if (@file_exists($ret)) {
          return $ret;
        }
      } catch(\Exception $e) {
        // fail silently
      }

      $defaults = ['/bin/'.$exe, '/usr/bin/'.$exe, '/usr/local/bin/'.$exe, '/usr/sbin/'.$exe, '/sbin/'.$exe];

      foreach($defaults as $default) {
        if (file_exists($default)) {
          return $default;
        }
      }

      throw new \Exception("Unable to locate suitable path for $exe");
    }

    static function move($from, $to) {
      $mv = static::getExecutablePath("mv");
      $cmd = sprintf("%s %s %s", $mv, escapeshellarg($from), escapeshellarg($to));
      static::execute($cmd);
    }

    static function unlink($src) {
      if (empty($src) || $src == "/") {
        throw new \Exception("Unable to perform delete.");
      }

      if (file_exists($src)) {
        if (is_dir($src)) {
          $rm = static::getExecutablePath("rm");
          $cmd = sprintf("%s -rf %s", $rm, escapeshellarg($src));
          static::execute($cmd);
        } else {
          $ret = @unlink($src);
          if (!$ret) {
            throw new \Exception("Unable to delete resource.");
          }
        }
      }
    }

    static function realpath($dir) {
      if (!empty($dir) && $dir[0] == "~" && isset($_SERVER["HOME"])) {
        $dir = $_SERVER["HOME"] . substr($dir, 1);
      }

      $path = @realpath($dir);
      return empty($path) ? $dir : $path;
    }

    static function download($url, $file) {
      $fp = @fopen($file, "w+");
      if (!$fp) {
        throw new \Exception("Unable to save file: $file");
      }
  
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch, CURLOPT_ENCODING, "gzip, deflate");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_FILE, $fp); 
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
  
      $buffer = @curl_exec($ch);
      curl_close($ch);
      fclose($fp);
  
      if ($buffer !== TRUE) {
        @unlink($file);
        throw new \Exception("Unable to download: $url");
      }
    }

    static function assertWritable($file) {
      if (!is_writable($file)) {
        throw new \Exception("Not writeable: $file");
      } 
    }

    static function uniqueId() {
      return md5(microtime(true).mt_rand());
    }

    static function mergePaths($a, $b) {
      $a = rtrim($a, "/");
      $b = ltrim($b, "/");
      return $a."/".$b;
    }
  }