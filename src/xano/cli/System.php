<?php
  namespace xano\cli;

  class System {
    static $RELEASE_TEMPLATE = '';

    static function mergePaths($a, $b) {
      $a = rtrim($a, "/");
      $b = ltrim($b, "/");
      return $a."/".$b;
    }

    static function realpath($dir) {
      if (!empty($dir) && $dir[0] == "~" && isset($_SERVER["HOME"])) {
        $dir = $_SERVER["HOME"] . substr($dir, 1);
      }

      $path = @realpath($dir);
      return empty($path) ? $dir : $path;
    }

    static function parseLines($a) {
      $a = trim($a);
      if (empty($a)) return [];
      $lines = explode("\n", $a);
      foreach($lines as &$line) {
        $line = trim($line);
      }
      return $lines;
    }

    static function hasUser() {
      return isset($_SERVER["XANO_USER"]);
    }

    static function getUser() {
      if (!isset($_SERVER["XANO_USER"])) {
        throw new \Exception("missing XANO_USER environment variable");
      }
      return $_SERVER["XANO_USER"];
    }

    static function getReleaseTemplate() {
      return self::$RELEASE_TEMPLATE;
    }

    static function setReleaseTemplate($tpl) {
      self::$RELEASE_TEMPLATE = $tpl;
    }

    static function applyEnv($env) {
      if (is_array($env)) {
        foreach($env as $key => $val) {
          $setting = is_int($key) ? $val : sprintf("%s=%s", $key, $val);
          putenv($setting);
          $_ENV[$key] = $val;
        }
      }
    }

    static function merge(array $A, array $B) {
      foreach ($B as $key => &$val) {
        if (is_array($val) && isset($A[$key]) && is_array($A[$key])) {
          $A[$key] = array_merge($A[$key], self::merge($A[$key], $val));
        } else {
          $A[$key] = $val;
        }
      }

      return $A;
    }

    static function getConfig() {
      $cfg = [];
      $curDir = getcwd();
      $file = sprintf("%s/profile/default.yaml", $curDir);

      if (file_exists($file)) {
        $data = static::readFile($file);
        $cfg = self::merge($cfg, Yaml::decode($data));
      }

      $file = sprintf("%s/profile/%s.yaml", $curDir, static::getUser());
      if (!file_exists($file)) {
        throw new \Exception("Unable to locate profile config: $file\nIs this the correct root directory of your repository?\nIf so, then create a profile config.");
      }

      $data = static::readFile($file);
      $cfg = self::merge($cfg, Yaml::decode($data));

      $release = self::getReleaseTemplate();
      if (!empty($release)) {
        $file = sprintf("%s/profile/release/%s.yaml", $curDir, $release);
        if (!file_exists($file)) {
          throw new \Exception("Unable to locate profile config: $file\nIs this the correct root directory of your repository?\nIf so, then create a profile config.");
        }
        $data = static::readFile($file);
        $cfg = self::merge($cfg, Yaml::decode($data));
      }

      return static::processConfig($cfg);
    }

    static function processConfig($cfg) {
      foreach(["instance","host","repo"] as $key) {
        if (!isset($cfg[$key])) {
          throw new \Exception("Missing entry \"$key\".");
        }
      }

      if (!isset($cfg["devlicense"]) && !isset($cfg["localcfg"])) {
        throw new \Exception("Missing local config entry.");
      }

      if (!is_array($cfg["repo"])) {
        $cfg["repo"] = [$cfg["repo"]];
      }

      static::walkConfig($cfg, $cfg);

      return $cfg;
    }

    private static function walkConfig($root, &$entry) {
      foreach($entry as &$val) {
        if (is_scalar($val)) {
          $origVal = $val;
          $lastVal = $val;
          $cnt = 0;
          while(true) {
            if ($cnt++ > 50) {
              throw new \Exception("Recursive variable: $origVal");
            }
            $matches = [];
            $ret = @preg_match_all("#\{([^}]+)\}#", $val, $matches);
            if ($ret !== FALSE && isset($matches[1])) {
              foreach($matches[1] as $match) {
                $val = str_replace("{".$match."}", static::evalConfig($root, $match), $val);
              }
              if ($lastVal == $val) break;
              $lastVal = $val;
              continue;
            }
            break;
          }
        } elseif (is_array($val)) {
          static::walkConfig($root, $val);
        }
      }
    }

    private static function evalConfig($cfg, $key) {
      if ($key[0] == "@") {
        return $_SERVER[substr($key, 1)] ?? "";
      }

      $parts = explode(".", $key);
      while(!empty($parts)) {
        $index = array_shift($parts);
        if (isset($cfg[$index])) {
          if (empty($parts)) return $cfg[$index];
          return static::evalConfig($cfg, implode(".", $parts));
        }
      }
      return "";
    }

    static function json_decode($value, $assoc = false) {
      $result = @json_decode($value, $assoc);
      if (JSON_ERROR_NONE !== json_last_error()) {
        throw new \Exception('Error parsing JSON: '.json_last_error_msg()."\n\n".$value);
      }
      return $result;
    }

    static function json_encode($value, $pretty = false) {
      $options = JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE;

      if ($pretty) {
        $options |= JSON_PRETTY_PRINT;
      }

      $result = @json_encode($value, $options);

      if ($result === FALSE) {
        throw new \Exception("Unable to encode object as json.");
      }

      return $result;
    }

    static function readFile($filename) {
      $data = @file_get_contents($filename);
      if ($data === FALSE) {
        throw new \Exception("Unable to read file: $filename");
      }
      return $data;
    }

    static function saveFile($filename, $data, $append = false) {
      $dir = pathinfo($filename, PATHINFO_DIRNAME);
      static::mkdir($dir);

      $ret = @file_put_contents($filename, $data, $append ? FILE_APPEND : 0);
      if ($ret === FALSE) {
        throw new \Exception("Unable to save file - $filename");
      }

      if (strlen($data) !== $ret) {
        @unlink($filename);
        throw new \Exception("Unable to save entire file - $filename. Out of disk space?"); 
      }
    }

    static function copyFile($src, $dst) {
      $dir = pathinfo($dst, PATHINFO_DIRNAME);
      static::mkdir($dir);

      $ret = @copy($src, $dst);
      if (!$ret) {
        throw new \Exception("Unable to copy file: $src to $dst");
      }
    }

    static function unlink($src) {
      if (empty($src) || $src == "/") {
        throw new \Exception("Unable to perform delete.");
      }

      if (file_exists($src)) {
        if (is_dir($src)) {
          $rm = static::getExecutablePath("rm");
          $cmd = sprintf("%s -rf %s", $rm, escapeshellarg($src));
          static::execute($cmd);
        } else {
          $ret = @unlink($src);
          if (!$ret) {
            throw new \Exception("Unable to delete resource.");
          }
        }
      }
    }

    static function mkdir($dir, $mode = 0755) {
      if (!file_exists($dir)) {
        $umask = umask(0);
        $ret = @mkdir($dir, $mode, true);
        umask($umask);

        if (!$ret) {
          throw new \Exception("Unable to create directory: $dir");
        }
      }
    }

    static function execute($cmd, $expectedCode = 0) {
      // printf("exe: %s\n", $cmd);
      $cmd2 = sprintf("%s 2>&1", $cmd);
      ob_start();
      $ret = 0;
      passthru($cmd2, $ret);
      $data = ob_get_clean();

      if (!is_null($expectedCode) && $ret !== $expectedCode) {
        throw new \Exception("Failed to execute: $cmd\n\nOutput:\n$data");
      }

      return $data;
    }

    static function passthru($cmd, $expectedCode = 0) {
      $cmd2 = sprintf("%s 2>&1", $cmd);
      $ret = 0;
      passthru($cmd2, $ret);

      if (!is_null($expectedCode) && $ret !== $expectedCode) {
        throw new \Exception("Failed to execute: $cmd\n\n");
      }

      return $ret;
    }

    static function getExecutablePath($exe) {
      $cmd = sprintf("which %s", escapeshellcmd($exe));
      try {
        $ret = static::execute($cmd);
        if (!empty($ret)) {
          if (strpos($ret, "alias") === 0) {
            $ret = explode($ret, "\n");
            unset($ret[0]);
            $ret = trim(implode("\n", $ret));
          } else {
            $ret = trim($ret);
          }
        }
  
        if (@file_exists($ret)) {
          return $ret;
        }
      } catch(\Exception $e) {
        // fail silently
      }

      $defaults = ['/bin/'.$exe, '/usr/bin/'.$exe, '/usr/local/bin/'.$exe, '/usr/sbin/'.$exe, '/sbin/'.$exe];

      foreach($defaults as $default) {
        if (file_exists($default)) {
          return $default;
        }
      }

      throw new \Exception("Unable to locate suitable path for $exe");
    }

    static function getBinaryName() {
      $parts = pathinfo($_SERVER["PHP_SELF"]);
      if (isset($parts["extension"])) return "xano";
      return $parts["filename"];
    }

    static function download($url, $file) {
      $fp = @fopen($file, "w+");
      if (!$fp) {
        throw new \Exception("Unable to save file: $file");
      }
  
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch, CURLOPT_ENCODING, "gzip, deflate");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_FILE, $fp); 
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
  
      $buffer = @curl_exec($ch);
      curl_close($ch);
      fclose($fp);
  
      if ($buffer !== TRUE) {
        @unlink($file);
        throw new \Exception("Unable to download: $url");
      }
    }

    static function uniqueId() {
      return md5(microtime(true).mt_rand());
    }

    static function getSelfCommand() {
      $self = $_SERVER["argv"][0];
      $parts = pathinfo($self);
      if (isset($parts["extension"])) {
        return [PHP_BINARY, $self];
      }

      return [$self];
    }

    static function getFiles($dir, $pattern = "*", $chdir = true) {
      if ($chdir) {
        $curDir = getcwd();
        $ret = @chdir($dir);
        if (!$ret) {
          return [];
        }
        $items = glob('*');
      } else {
        $items = glob("$dir/*");
      }

      $result = [];

      foreach($items as $item) {
        if (is_dir($item)) {
          $result = array_merge($result, self::getFiles($item, $pattern, false));
        } elseif (fnmatch($pattern, $item)) {
          $result[] = $item;
        }
      }

      if ($chdir) {
        chdir($curDir);
      }
      return $result;
    }
  }