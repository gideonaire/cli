<?php
  namespace xano\cli;

  class Yaml  {
    static function decode(string $value, array $tags = []) {
      $lines = [];
      $max = 100000;
      foreach(explode("\n", $value) as $line) {
        if (empty(trim($line))) continue;
        $lines[] = $line;

        $len = strlen($line) - strlen(ltrim($line));
        if ($len < $max) {
          $max = $len;
        }
      }

      if ($max > 0) {
        foreach($lines as &$line) {
          $line = substr($line, $max);
        }
      }

      $value = implode("\n", $lines);

      $options = \Symfony\Component\Yaml\Yaml::PARSE_CUSTOM_TAGS;

      $result = \Symfony\Component\Yaml\Yaml::parse(
        $value, 
        $options
      );

      return $result;
    }

    static function encode($value, $prefix = '') {
      $str = \Symfony\Component\Yaml\Yaml::dump(
        $value, 
        2, 
        4, 
        \Symfony\Component\Yaml\Yaml::DUMP_OBJECT_AS_MAP | \Symfony\Component\Yaml\Yaml::DUMP_EMPTY_ARRAY_AS_SEQUENCE
      );

      if (!empty($prefix)) {
        $lines = explode("\n", $str);
        foreach($lines as &$line) {
          $line = $prefix.$line;
        }
        $str = implode("\n", $lines);
      }

      return $str;
    }
  }