<?php
  namespace xano\cli;

  abstract class Command {
    function isHidden() {
      return false;
    }
    
    function getComposerDependencies() {
      return [];
    }

    abstract function getName();
    abstract function getUsage();
    abstract function getOptions();
    abstract function run(\xano\cli\App $app, array $params);
  }