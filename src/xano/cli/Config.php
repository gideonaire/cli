<?php
  namespace xano\cli;

  class Config {
    const VERSION = "2.07";
    const WATCHMAN_PREFIX = "xano-";
    const WATCHMAN_TRIGGER = "xanorun";
    const COMPOSER_VENDOR_PATH = "xano_modules/bin/vendor";
    const WATCHMAN_TRIGGER_FILE = ".watchman-triggerfile.log";
    const BUILD_IGNORE_PATH = "__IGNORE__";
  }