<?php

  namespace xano\cli\command;

  use \xano\cli\Config as Config;
  use \xano\cli\System as System;

  class InstallDB extends \xano\cli\Command {
    function getName() {
      return "install-db";
    }

    function getUsage() {
      return "install/update database schema";
    }

    function getOptions() {
      return [
        (new \xano\cli\Option())
          ->name("refresh-ext")
          ->type("text")
          ->usage("an extension name to safely re-install missing dependencies."),
        (new \xano\cli\Option())
          ->name("local")
          ->type("bool")
          ->usage("perform installation locally... this requires a successful db connection to remote server."),
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      printf("installing/updating database schema... one moment\n");
      $cfg = System::getConfig();

      $ssh = System::getExecutablePath("ssh");

      if (isset($params["local"])) {
        $app->process("build", ["-grep", Config::BUILD_IGNORE_PATH]);
        $cmd = sprintf("%s xano_modules/bin/tools/installExtensions.php",
          PHP_BINARY
        );
      } else {
        $cmd = sprintf("%s dev@%s php /xano/%s/dev/%s/bin/tools/installExtensions.php",
          $ssh,
          $cfg["host"],
          $cfg["remote_instance"] ?? $cfg["instance"],
          $cfg["user"] ?? System::getUser()
        );
      }

      if (isset($params["refresh-ext"])) {
        $cmd .= sprintf(" --refresh-ext=%s", escapeshellarg($params["refresh-ext"]));
      }

      System::passthru($cmd);
    }
  }