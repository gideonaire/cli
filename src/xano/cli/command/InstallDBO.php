<?php

  namespace xano\cli\command;

  use \xano\cli\Config as Config;
  use \xano\cli\System as System;

  class InstallDBO extends \xano\cli\Command {
    function getName() {
      return "install-dbo";
    }

    function getUsage() {
      return "install/update dbo specific schema";
    }

    function getOptions() {
      return [
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      $app->process("install-db", [
        "-local",
        "-refresh-ext",
        "DBO"
      ]);
    }
  }