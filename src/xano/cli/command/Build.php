<?php

  namespace xano\cli\command;

  use \xano\cli\Config as Config;
  use \xano\cli\System as System;
  use \xano\cli\Yaml as Yaml;

  class Build extends \xano\cli\Command {
    function getName() {
      return "build";
    }

    function getUsage() {
      return "create a local build - development or production";
    }

    function getOptions() {
      return [
        (new \xano\cli\Option())
          ->name("grep")
          ->type("text")
          ->usage("pattern to filter directories for a partial build"),
        (new \xano\cli\Option())
          ->name("p")
          ->type("bool")
          ->usage("production build - optimizations enabled")
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      $__START = microtime(true);

      $app->process("prepare", []);

      $find = System::getExecutablePath("find");
      $prepDir = sprintf("%s/xano_modules/_/", getcwd());
      $binDir = sprintf("%s/xano_modules/bin/", getcwd());
      $install = System::realpath(System::mergePaths($prepDir, "xano-install.sh"));
      if (file_exists($install)) {
        $yarnLock = System::realpath(System::mergePaths($prepDir, "yarn.lock"));
        $yarnLastLock = System::realpath(System::mergePaths($prepDir, ".last-yarn.lock"));
        if (@md5_file($yarnLock) !== @md5_file($yarnLastLock)) {
          $app->process("install", []);
          clearstatcache();
        }
      }

      $cfg = System::getConfig();

      if (isset($params["p"])) {
        echo "******************\n";
        echo "*** PROD BUILD ***\n";
        echo "******************\n\n";
      } else {
        echo "*****************\n";
        echo "*** DEV BUILD ***\n";
        echo "*****************\n\n";
      }

      if (isset($cfg["env"]["build"])) {
        System::applyEnv($cfg["env"]["build"]);
      }

      $dirs = [];
      $dirs[] = escapeshellarg(System::realpath(System::mergePaths($prepDir, "extensions")));

      $cmd = sprintf("%s %s -maxdepth 2 -type f 2>/dev/null | grep xano-build.sh",
        $find,
        implode(" ", $dirs)
      );

      if (($params["grep"] ?? "") != \xano\cli\Config::BUILD_IGNORE_PATH) {
        if (isset($params["grep"])) {
          $cmd .= sprintf(" | grep %s", escapeshellarg($params["grep"]));
          printf("filtering: %s\n", $params["grep"]);
        }

        $result = System::execute($cmd, null);

        $files = System::parseLines($result);
        foreach($files as $file) {
          $fileDir = pathinfo($file, PATHINFO_DIRNAME);

          $yarnLock = System::realpath(System::mergePaths($fileDir, "yarn.lock"));
          $yarnLastLock = System::realpath(System::mergePaths($fileDir, ".last-yarn.lock"));
          if (@md5_file($yarnLock) !== @md5_file($yarnLastLock)) {
            $app->process("install", []);
          }

          $cmd = $file;
          if (!isset($params["p"])) {
            $cmd .= " dev";
          }

          $__RUN_TIME = microtime(true);
          printf("running: %s ", $cmd);
          System::execute($cmd);
          printf("%.2fs\n", microtime(true) - $__RUN_TIME);
        }
      }

      printf("packaging... one moment\n");

      $cmd = sprintf("%s %s -maxdepth 1 -type d 2>/dev/null | grep -e \"extensions/\"",
        $find,
        implode(" ", $dirs)
      );

      $result = System::execute($cmd);
      $files = System::parseLines($result);

      $rsync = System::getExecutablePath("rsync");
      $cmd = sprintf("%s -rlptDvK --inplace --delete ",
        $rsync
      );

      $cmd .= sprintf("--exclude=.* --exclude=xano-*.sh --exclude=loader.php --exclude=yarn* --exclude=package.json --exclude=webpack.config.js --exclude=/composer* --exclude=/vendor/ --exclude=/node_modules/ --exclude=/xano_modules/ ");

      $setupDir = System::realpath(System::mergePaths($prepDir, "setup"));
      if (file_exists($setupDir)) {
        $cmd .= sprintf("%s ", escapeshellarg($setupDir));
      }

      foreach($files as $file) {
        $cmd .= sprintf("%s ", escapeshellarg(System::realpath($file)."/"));
      }

      System::mkdir($binDir);

      $cmd .= $binDir;

      System::execute($cmd);

      $node_modules = sprintf("%s/node_modules", $prepDir);
      if (file_exists($node_modules)) {
        $cmd = sprintf("%s -rlptDvK --inplace --delete %s %s",
          $rsync,
          $node_modules,
          $binDir
        );
        System::execute($cmd);
      }

      $cmd = sprintf("%s %s %s",
        PHP_BINARY,
        escapeshellarg(sprintf("%s/xano_modules/bin/setup/install.php", getcwd())),
        escapeshellarg(sprintf("%s/xano_modules/bin/", getcwd()))
      );

      System::passthru($cmd);

      if (isset($cfg["devlicense"])) {
        $cfgDir = sprintf("%s/xano_modules/storage/cfg", getcwd());
        System::mkdir($cfgDir);
        $licFile = sprintf("%s/xano_modules/storage/cfg/license.config", getcwd());
        System::saveFile($licFile, System::json_encode($cfg["devlicense"], true));
      } elseif (isset($cfg["localcfg"])) {
        $licFile = sprintf("%s/xano_modules/bin/xano.yaml", getcwd());
        System::saveFile($licFile, Yaml::encode($cfg["localcfg"]));
      } else {
        throw new \Exception("Missing local config.");
      }

      printf("build complete: %.2fs\n", microtime(true) - $__START);
    }
  }