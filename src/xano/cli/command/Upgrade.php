<?php

  namespace xano\cli\command;

  use \xano\cli\System as System;

  class Upgrade extends \xano\cli\Command {
    function getName() {
      return "upgrade";
    }

    function getUsage() {
      return "upgrade build dependencies to their latest version";
    }

    function getOptions() {
      return [
        (new \xano\cli\Option())
          ->name("grep")
          ->type("text")
          ->usage("pattern to filter directories for a partial upgrade"),
        (new \xano\cli\Option())
          ->name("pkg")
          ->type("text")
          ->usage("package to ugprade"),
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      $app->process("prepare", []);

      $curDir = getcwd();

      $prepDir = sprintf("%s/xano_modules/_/", $curDir);

      if (!isset($params["pkg"])) {
        printf("### WARNING ###########################\n");
        printf("This is dangerous and will update ALL packages.\nHINT: You may want to just upgrade a single package via the -pkg parameter.\n\n");
        printf("Are you sure you want to do this? If so, type: yes\n");
        $num = readline("prompt: ");
        if ($num != "yes") {
          printf("aborting.\n");
          exit(1);
        }
      }

      $cfg = System::getConfig();

      if (empty($params["grep"] ?? "")) {
        $file = System::mergePaths($prepDir, "xano-upgrade.sh");
        if (file_exists($file)) {
          printf("running: %s\none moment...\n", $file);
          System::execute($file);

          $yarnLock = "yarn.lock";

          System::copyFile(
            System::mergePaths($prepDir, $yarnLock),
            System::mergePaths($curDir, $yarnLock)
          );
        }
      }

      $find = System::getExecutablePath("find");

      foreach($cfg["repo"] as $repo) {
        $repo = System::realpath($repo);
        $dir = "extensions";

        $cmd = sprintf("%s %s -maxdepth 2 -type f | grep xano-upgrade.sh", $find, $dir);

        if (($params["grep"] ?? "") != \xano\cli\Config::BUILD_IGNORE_PATH) {
          if (isset($params["grep"])) {
            $cmd .= sprintf(" | grep %s", escapeshellarg($params["grep"]));
          }
        }

        $chdir = getcwd();
        chdir($repo);
        $result = System::execute($cmd, null);
        chdir($chdir);

        $files = System::parseLines($result);
        foreach($files as $file) {
          $cmd = System::mergePaths($prepDir, $file);
          if (isset($params["pkg"])) {
            $cmd .= sprintf(" %s", escapeshellarg($params["pkg"]));
          }
  
          printf("running: %s\none moment...\n", $cmd);
          $ret = System::execute($cmd);
  
          if (isset($params["pkg"])) {
            $ret = @preg_match("#Saved\s+(\d+)\s+new\s+dependencies#", $ret, $matches);
            if (isset($matches[1]) && $matches[1] == 0) {
              printf("ERROR: No update was performed. Double check the spelling of your package.\n");
              exit(1);
            }
          }

          $parts = pathinfo($file);
          $yarnLock = sprintf("%s/yarn.lock", $parts["dirname"]);

          System::copyFile(
            System::mergePaths($prepDir, $yarnLock),
            System::mergePaths($repo, $yarnLock)
          );
        }
      }

      printf("\ndone\n");
    }
  }