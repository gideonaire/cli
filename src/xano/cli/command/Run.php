<?php

  namespace xano\cli\command;

  use \xano\cli\Config as Config;
  use \xano\cli\System as System;

  class Run extends \xano\cli\Command {
    function getName() {
      return "run";
    }

    function getUsage() {
      return "run local xano environment";
    }

    function getOptions() {
      return [
        (new \xano\cli\Option())
          ->name("grep")
          ->type("text")
          ->usage("pattern to filter directories for a partial build"),
        (new \xano\cli\Option())
          ->name("host")
          ->type("text")
          ->usage("allows you to use a host:port other than the default localhost:9000"),
        (new \xano\cli\Option())
          ->name("nobuild")
          ->type("bool")
          ->usage("skip build prior to run"),
        (new \xano\cli\Option())
          ->name("nobuild-once")
          ->type("bool")
          ->usage("skip build prior to run (only once)"),
        (new \xano\cli\Option())
          ->name("p")
          ->type("bool")
          ->usage("production build - optimizations enabled")
      ];
    }

    function onCleanup() {
      $cfg = System::getConfig();
      $this->cleanupTriggers($cfg["repo"]);

      die("\nexiting...\n");
    }

    function run(\xano\cli\App $app, array $params) {
      pcntl_signal(SIGTERM, [$this, "onCleanup"]);
      pcntl_signal(SIGHUP,  [$this, "onCleanup"]);
      pcntl_signal(SIGUSR1, [$this, "onCleanup"]);
      pcntl_signal(SIGINT,  [$this, "onCleanup"]);

      $originalGrep = $params["grep"] ?? null;

      $first = true;
      while(true) {
        if (!isset($params["nobuild"]) && !isset($params["nobuild-once"])) {
          if ($first) printf("building first...\n\n");
          $args = [];
          if (isset($params["grep"])) {
            $args[] = "-grep";
            $args[] = $params["grep"];
          }
          if (isset($params["p"])) {
            $args[] = "-p";
          }
          $app->process("build", $args);
        } else {
          // printf("skipping build...\n");
          unset($params["nobuild-once"]);
        }

        $cfg = System::getConfig();

        $binDir = sprintf("%s/xano_modules/bin/", getcwd());
        if (!file_exists($binDir)) {
          throw new \Exception("No build present. A successful build is necessary for run.");
        }

        try {
          $watchman = System::getExecutablePath("watchman");
          if ($first) {
            // printf("watchman detected - updates will automatically restart environment\n");
          }
        } catch(\Exception $e) {
          // ignore - no watchman
          // printf("no watchman detected... updates require a manual restart for updates\n");
        }

        $first = false;

        $killTag = System::uniqueId();

        if (isset($watchman)) {
          $tmpFile = sprintf("%s/tmp_%s.json", getcwd(), System::uniqueId());

          try {
            $this->cleanupTriggers($cfg["repo"]);
            foreach($cfg["repo"] as $repo) {
              $path = System::realpath($repo);
              $trigger = $this->createTrigger(Config::WATCHMAN_TRIGGER, $path, $killTag);
              System::saveFile($tmpFile, $trigger);

              $cmd = sprintf('%s -j > /dev/null < %s', $watchman, $tmpFile);
              System::execute($cmd);
            }
          } finally {
            System::unlink($tmpFile);
          }
        } 

        $host = $params["host"] ?? "localhost:9000";

        printf("\n*** SUCCESS - local server: http://%s/\n", $host);

        $cmd = sprintf("%s -S %s %s -- %s",
          PHP_BINARY,  
          escapeshellarg($host),
          escapeshellarg(sprintf("%s/xano_modules/bin/local-project.php", getcwd())),
          Config::WATCHMAN_PREFIX.$killTag
        );
        
        $ret = System::passthru($cmd, null);
        switch($ret) {
          case 15: {
            if (isset($watchman)) {
              printf("\n*** UPDATE DETECTED - REBUILDING\n\n");

              $this->cleanupTriggers($cfg["repo"]);

              try {
                $file = sprintf("%s/%s", sys_get_temp_dir(), Config::WATCHMAN_TRIGGER_FILE);
                $data = System::readFile($file);
                System::unlink($file);

                $ext = pathinfo($data, PATHINFO_EXTENSION);
                if (in_array($ext, ["php","yaml"])) {
                  $params["grep"] = Config::BUILD_IGNORE_PATH;
                } else {
                  $params["grep"] = $originalGrep;
                }
              } catch(\Exception $e) {
                // ignore
              }

              if (isset($params["nobuild"])) {
                printf("exiting due to -nobuild being set.\n");
                return;
              }

              continue 2;
            }

          } break;
        }

        break;
      }
    }

    function cleanupTriggers($repos) {
      try {
        $watchman = System::getExecutablePath("watchman");

        foreach($repos as $repo) {
          $path = System::realpath($repo);

          $cmd = sprintf("%s watch-del %s",
            $watchman,
            escapeshellarg($path)
          );
          System::execute($cmd);
        }
      } catch(\Exception $e) {
        // no watchman
      }
    }

    function createTrigger($name, $dir, $killTag) {
      $cmd = System::getSelfCommand();
      $cmd[] = "watchman-trigger";
      $cmd[] = "-kill";
      $cmd[] = $killTag;

      $obj = [];
      $obj[] = "trigger";
      $obj[] = $dir;
      $obj[] = [
        "name" => $name,
        "expression" => [
          "anyof",
          ["match", "**/*", "wholename"]
        ],
        "append_files" => true,
        "command" => $cmd
      ];

      return json_encode($obj);
    }
  }