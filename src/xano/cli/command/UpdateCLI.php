<?php

  namespace xano\cli\command;

  use \xano\cli\System as System;

  class UpdateCLI extends \xano\cli\Command {
    function getName() {
      return "update-cli";
    }

    function getUsage() {
      return "update xano-cli to the latest version";
    }

    function getOptions() {
      return [
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      $parts = pathinfo($_SERVER["PHP_SELF"]);

      $curl = System::getExecutablePath("curl");
      $cmd = sprintf("%s -s %s", $curl, escapeshellarg("https://gitlab.com/xano/cli/raw/master/dist/setup-xanocli.php"));

      $cmd .= sprintf(" | %s", PHP_BINARY);
      if (!isset($parts["extension"])) {
        // running from standard install
        $cmd .= sprintf(" -- --install-dir=%s --filename=%s", escapeshellarg($parts["dirname"]),  escapeshellarg($parts["basename"]));
      }

      // ignore return code since we see the result - we don't want to add more error notices
      System::passthru($cmd, null);
      printf("\nInstallation is complete. You are now running:\n");
      System::passthru("xano version", null);
      printf("\n");
    }
  }