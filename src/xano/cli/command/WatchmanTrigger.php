<?php

  namespace xano\cli\command;

  use \xano\cli\Config as Config;
  use \xano\cli\System as System;

  class WatchmanTrigger extends \xano\cli\Command {
    function isHidden() {
      return true;
    }

    function getName() {
      return "watchman-trigger";
    }

    function getUsage() {
      return "";
    }

    function getOptions() {
      return [
        (new \xano\cli\Option())
          ->name("kill")
          ->type("text")
          ->usage("pattern to kill process"),
        (new \xano\cli\Option())
          ->name("files")
          ->match("*")
          ->prefix("")
          ->type("text")
          ->usage("")
          ->required(),
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      $files = $params["files"];
      if (count($files) === 1 && is_file($files[0])) {
        foreach(["xano_modules","node_modules"] as $findMe) {
          if (strpos($files[0], $findMe) !== FALSE) return;
        }

        $file = sprintf("%s/%s", sys_get_temp_dir(), Config::WATCHMAN_TRIGGER_FILE);
        System::saveFile($file, $files[0]);

        $pkill = System::getExecutablePath("pkill");
        $cmd = sprintf("%s -f %s", $pkill, escapeshellarg(Config::WATCHMAN_PREFIX.$params["kill"]));
        System::execute($cmd, null);
      }
    }
  }