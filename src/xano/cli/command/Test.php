<?php

  namespace xano\cli\command;

  use \xano\cli\Config as Config;
  use \xano\cli\System as System;

  class Test extends \xano\cli\Command {
    function getName() {
      return "test";
    }

    function getUsage() {
      return "run tests";
    }

    function getOptions() {
      return [
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      $app->process("build", [
        "-grep",
        Config::BUILD_IGNORE_PATH
      ]);

      $testFile = sprintf("%s/xano_modules/bin/tools/runTests.php", getcwd());
      $cmd = sprintf("%s %s",
        PHP_BINARY,  
        escapeshellarg($testFile)
      );

      printf("running tests... one moment\n\n");

      exit(System::passthru($cmd, null));
    }
  }