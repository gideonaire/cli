<?php

  namespace xano\cli\command;

  use \xano\cli\System as System;

  class Help extends \xano\cli\Command {
    function getName() {
      return "help";
    }

    function getUsage() {
      return sprintf("%s help [ACTION]", System::getBinaryName());
    }

    function getOptions() {
      return [
        (new \xano\cli\Option())
          ->name("action")
          ->match("*")
          ->prefix("")
          ->type("text")
          ->usage("the action needing additional information")
          ->required(),
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      $action = $params["action"][0];
      $cmd = $app->getCommand($action);

      printf("Usage: %s %s [OPTIONS]\n", System::getBinaryName(), $action);
      printf(" %s\n\n", $cmd->getUsage());

      $options = $cmd->getOptions();
      if (empty($options)) {
        printf("There are no additional options for this action.\n");
      } else {
        printf("Available Options:\n");

        foreach($options as $opt) {
          printf("  %s: %s; %s\n", $opt->getMatch(), $opt->isRequired() ? "required" : "optional", $opt->getUsage());
        }
      }

      printf("\n");
    }
  }