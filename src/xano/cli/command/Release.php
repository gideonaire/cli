<?php

  namespace xano\cli\command;

  use \xano\cli\System as System;

  class Release extends \xano\cli\Command {
    function getName() {
      return "release";
    }

    function getUsage() {
      return "publish release to server";
    }

    function getOptions() {
      return [
        (new \xano\cli\Option())
          ->name("t")
          ->type("text")
          ->usage("release template")
          ->required(),
        (new \xano\cli\Option())
          ->name("full")
          ->type("bool")
          ->usage("run all optional cmds")
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      System::setReleaseTemplate($params["t"]);
      
      $cfg = System::getConfig();

      $cmds = $cfg["cmds"] ?? [];
      if (empty($cmds)) {
        throw new \Exception("Release is missing cmds block.");
      }

      foreach($cmds as $cmd) {
        if (!is_array($cmd) || !isset($cmd["action"]) || !isset($cmd["args"])) {
          throw new \Exception("Invalid cmd format.");
        }

        if (($cmd["required"] ?? false) || ($params["full"] ?? false)) {
          $app->process($cmd["action"], $cmd["args"]);
        }
      }
    }
  }