<?php

  namespace xano\cli\command;

  use \xano\cli\System as System;

  class Clean extends \xano\cli\Command {
    function getName() {
      return "clean";
    }

    function getUsage() {
      return "clean temporary files";
    }

    function getOptions() {
      return [
        (new \xano\cli\Option())
          ->name("full")
          ->type("bool")
          ->usage("full clean - 'xano install' must be run afterwards before another build")
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      printf("cleaning temporary files...\n");

      $file = sprintf("%s/xano-clean.sh", getcwd());
      if (file_exists($file)) {
        System::execute($cmd);
      }

      System::unlink(getcwd()."/xano_modules/bin");
  
      $find = System::getExecutablePath("find");

      $cfg = System::getConfig();

      $dirs = [];
      foreach($cfg["repo"] as $repo) {
        $dirs[] = System::mergePaths($repo, "extensions");
      }

      $cmd = sprintf("%s %s -maxdepth 2 -type f | grep xano-clean.sh", $find, implode(" ", $dirs));
      $result = System::execute($cmd, null);
      $files = System::parseLines($result);

      foreach($files as $file) {
        System::execute($file);
      }

      $cmd = sprintf("%s %s %s -maxdepth 2 -type d | grep -E \"%s\"", 
        $find,
        getcwd(),
        implode(" ", $dirs),
        isset($params["full"]) ? "node_modules|xano_modules" : "xano_modules"
      );

      $result = System::execute($cmd, null);
      $files = System::parseLines($result);
      foreach($files as $file) {
        System::unlink($file);
      }

      if (isset($params["full"])) {
        printf("\ndone; make sure to run a 'xano install' before your next build.\n\n");
      } else {
        printf("\ndone\n");
      }
    }
  }