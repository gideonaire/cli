<?php

  namespace xano\cli\command;

  use \xano\cli\System as System;

  class Install extends \xano\cli\Command {
    function getName() {
      return "install";
    }

    function getUsage() {
      return "install build dependencies";
    }

    function getOptions() {
      return [
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      $app->process("prepare", []);
      $curDir = getcwd();

      $prepDir = sprintf("%s/xano_modules/_/", $curDir);

      $file = System::mergePaths($prepDir, "xano-install.sh");
      if (file_exists($file)) {
        printf("running: %s\n", $file);
        System::execute($file);

        $yarnLock = "yarn.lock";
        $yarnLastLock = ".last-yarn.lock";

        System::copyFile(
          System::mergePaths($prepDir, $yarnLock),
          System::mergePaths($prepDir, $yarnLastLock)
        );

        System::copyFile(
          System::mergePaths($prepDir, $yarnLock),
          System::mergePaths($curDir, $yarnLock)
        );
      }

      $find = System::getExecutablePath("find");
      $cfg = System::getConfig();

      foreach($cfg["repo"] as $repo) {
        $repo = System::realpath($repo);
        $dir = "extensions";

        $cmd = sprintf("%s %s -maxdepth 2 -type f | grep xano-install.sh", $find, $dir);

        $chdir = getcwd();
        chdir($repo);
        $result = System::execute($cmd, null);
        chdir($chdir);

        $files = System::parseLines($result);
        foreach($files as $file) {
          $cmd = System::mergePaths($prepDir, $file);
  
          printf("running: %s\none moment...\n", $cmd);
          System::execute($cmd);

          $parts = pathinfo($file);
          $yarnLock = sprintf("%s/yarn.lock", $parts["dirname"]);
          $yarnLastLock = sprintf("%s/.last-yarn.lock", $parts["dirname"]);

          System::copyFile(
            System::mergePaths($prepDir, $yarnLock),
            System::mergePaths($prepDir, $yarnLastLock)
          );

          System::copyFile(
            System::mergePaths($prepDir, $yarnLock),
            System::mergePaths($repo, $yarnLock)
          );
        }
      }

      printf("\ndone\n");
    }
  }