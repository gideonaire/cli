<?php

  namespace xano\cli\command;

  use \xano\cli\Config as Config;
  use \xano\cli\System as System;

  class Sync extends \xano\cli\Command {
    function getName() {
      return "sync";
    }

    function getUsage() {
      return "sync local build to server";
    }

    function getOptions() {
      return [
        (new \xano\cli\Option())
          ->name("nobuild")
          ->type("bool")
          ->usage("skip build prior to sync"),
          (new \xano\cli\Option())
          ->name("p")
          ->type("bool")
          ->usage("production build - optimizations enabled")
        ];
      }
      
    function run(\xano\cli\App $app, array $params) {
      if (!isset($params["nobuild"])) {
        printf("building first...\n\n");
        $args = [];
        if (isset($params["p"])) {
          $args[] = "-p";
        }
        $app->process("build", $args);
      } else {
        $app->process("build", [
          "-grep",
          Config::BUILD_IGNORE_PATH
        ]);
      }

      $__START = microtime(true);
      $cfg = System::getConfig();

      printf("syncing...\n");

      $binDir = sprintf("%s/xano_modules/bin/", getcwd());

      if (!file_exists($binDir)) {
        throw new \Exception("No build present. A successful build is necessary for sync.");
      }

      $remoteBinName = sprintf("bin-%d", time());

      $userDir = sprintf("/xano/%s/dev/%s",
        $cfg["remote_instance"] ?? $cfg["instance"],
        $cfg["user"] ?? System::getUser()
      );

      $oldRemoteBinDir = sprintf("%s/bin", $userDir);
      $remoteTmpBinDir = sprintf("%s/storage/tmp/bin", $userDir);
      $tmpRemoteBinDir = sprintf("%s/replace-%s", $remoteTmpBinDir, $remoteBinName);
      $newRemoteBinDir = sprintf("%s/%s", $remoteTmpBinDir, $remoteBinName);

      $ssh = System::getExecutablePath("ssh");
      $remoteCmd = sprintf("mkdir -p %s && mkdir -p %s && cp -Rp %s %s", 
        escapeshellarg($oldRemoteBinDir),
        escapeshellarg($remoteTmpBinDir),
        escapeshellarg($oldRemoteBinDir."/"),
        escapeshellarg($newRemoteBinDir)
      );

      $cmd = sprintf("%s dev@%s %s", 
        $ssh, 
        $cfg["host"],
        escapeshellarg($remoteCmd)
      );

      System::execute($cmd);

      $rsync = System::getExecutablePath("rsync");
      $cmd = sprintf("%s -rlptDvzK --exclude=.* --delete %s dev@%s:%s/",
        $rsync,
        escapeshellarg($binDir),
        $cfg["host"],
        $newRemoteBinDir
      );

      System::passthru($cmd);

      $ssh = System::getExecutablePath("ssh");
      $remoteCmd = sprintf("mv %s %s && mv %s %s && rm -rf %s", 
        escapeshellarg($oldRemoteBinDir),
        escapeshellarg($tmpRemoteBinDir),
        escapeshellarg($newRemoteBinDir),
        escapeshellarg($oldRemoteBinDir),
        escapeshellarg($tmpRemoteBinDir)
      );

      $cmd = sprintf("%s dev@%s %s", 
        $ssh, 
        $cfg["host"],
        escapeshellarg($remoteCmd)
      );

      System::execute($cmd);

      printf("sync complete: %.2fs\n", microtime(true) - $__START);
    }
  }