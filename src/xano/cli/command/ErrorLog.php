<?php

  namespace xano\cli\command;

  use \xano\cli\System as System;

  class ErrorLog extends \xano\cli\Command {
    function getName() {
      return "error";
    }

    function getUsage() {
      return "prints the errors for the current project";
    }

    function getOptions() {
      return [
        (new \xano\cli\Option())
          ->name("lines")
          ->type("integer")
          ->usage("amount of lines of the error log to print (default: 50)")
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      $errorlog = ini_get("error_log");
      if (!empty($errorlog) && file_exists($errorlog)) {
        printf("########################\n");
        printf("### SYSTEM ERROR LOG ###\n");
        printf("########################\n\n");
        $cmd = sprintf("tail -n %s %s",
          10,
          escapeshellarg($errorlog)
        );
        System::passthru($cmd);
      }

      $cmd = sprintf("tail -n %s %s",
        escapeshellarg($params["lines"] ?? 50),
        escapeshellarg(getcwd().'/xano_modules/storage/tmp/error.log')
      );

      printf("\n##########################\n");
      printf("### XANO ERROR LOG #######\n");
      printf("##########################\n\n");
      System::passthru($cmd);
    }
  }