<?php

  namespace xano\cli\command;

  use \xano\cli\System as System;

  class Add extends \xano\cli\Command {
    function getName() {
      return "add";
    }

    function getUsage() {
      return "add build dependency";
    }

    function getOptions() {
      return [
        (new \xano\cli\Option())
          ->name("ext")
          ->type("text")
          ->usage("extension name")
          ->required(),
        (new \xano\cli\Option())
          ->name("pkg")
          ->type("text")
          ->usage("package to ugprade")
          ->required()
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      $app->process("prepare", []);

      $curDir = getcwd();

      $prepDir = sprintf("%s/xano_modules/_/", $curDir);

      $cfg = System::getConfig();

      foreach($cfg["repo"] as $repo) {
        $repo = System::realpath($repo);
        $dir = "extensions";

        $extDir = System::mergePaths($repo, $dir);
        $extDir = System::mergePaths($extDir, $params["ext"]);

        if (is_dir($extDir)) {
          $found = true;
          break;
        }
      }

      if (!isset($found)) {
        throw new \Exception("Unable to locate extension.");
      }

      $yarn = System::getExecutablePath("yarn");

      $prepExtDir = System::mergePaths($prepDir, $dir);
      $prepExtDir = System::mergePaths($prepExtDir, $params["ext"]);
      $chdir = getcwd();
      chdir($prepExtDir);

      $cmd = sprintf("%s add %s", $yarn, escapeshellarg($params["pkg"]));
      System::passthru($cmd);

      System::copyFile(
        System::mergePaths($prepExtDir, "yarn.lock"),
        System::mergePaths($extDir, "yarn.lock")
      );

      System::copyFile(
        System::mergePaths($prepExtDir, "package.json"),
        System::mergePaths($extDir, "package.json")
      );

      printf("\ndone\n");
    }
  }