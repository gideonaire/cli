<?php

  namespace xano\cli\command;

  use \xano\cli\System as System;

  class Version extends \xano\cli\Command {
    function getName() {
      return "version";
    }

    function getUsage() {
      return "display version information";
    }

    function getOptions() {
      return [];
    }

    function run(\xano\cli\App $app, array $params) {
      printf("%s\n", \xano\cli\Config::VERSION);
    }
  }