<?php
  namespace xano\cli;

  class App {
    private $cmds;
    private $action;
    private $args;
    private $options;

    function __construct(array $args) {
      $this->cmds = [];
      $this->action = array_shift($args);
      $this->args = $args;

      $this->options = [
        (new \xano\cli\Option())
          ->name("u")
          ->type("text")
          ->usage("force user profile"),
      ];

      pcntl_async_signals(true);
    }

    function getComposerDependencies() {
      return [
        "symfony/yaml" => "^4.1",
      ];
    }

    function scan() {
      $dirs = [];
      $paths = explode(":", get_include_path());

      foreach($paths as $path) {
        $dirs[] = \xano\cli\System::mergePaths(
          $path == "." ? getcwd() : $path,
          "xano/cli/command"
        );
      }

      $dirs = array_unique($dirs);

      $files = [];
      foreach($dirs as $dir) {
        $result = glob("$dir/*.php");
        if (!empty($result)) {
          foreach($result as $file) {
            $className = pathinfo($file, PATHINFO_FILENAME);
            $class = '\xano\cli\command\\'.$className;
            // only register valid commands
            if (class_exists($class) && is_subclass_of($class, '\xano\cli\Command')) {
              $this->registerCommand(new $class);
            }
          }
        }
      }
    }

    function getCommand($name) {
      foreach($this->cmds as $key => $cmd) {
        if ($name == $key) return $cmd;
      }

      throw new \Exception(
        sprintf("Unable to locate any action by that name.")
      );
    }

    function registerCommand(\xano\cli\Command $cmd) {
      $this->cmds[$cmd->getName()] = $cmd;
    }

    function processComposer() {
      $cliDir = sprintf("%s/xano_modules/cli", getcwd());
      $composerVendorDir = sprintf("%s/vendor", $cliDir);
      $composer = $this->generateComposer($composerVendorDir);
      $autoloadFile = sprintf("%s/autoload.php", $composerVendorDir);

      $composerFile = sprintf("%s/composer.json", $cliDir);
      $composerLock = sprintf("%s/composer.lock", $cliDir);
      $composerConfig = json_encode($composer, JSON_PRETTY_PRINT);
      $composerSigA1 = sha1($composerConfig);
      $composerSigA2 = file_exists($composerFile) ? sha1_file($composerFile) : 'A2';
      if ($composerSigA1 !== $composerSigA2 || !file_exists($autoloadFile)) {
        $composer = System::getExecutablePath("composer");
        System::saveFile($composerFile, $composerConfig);

        printf("installing composer dependencies...\n");
        $cmd = sprintf("%s install -n -d %s", $composer, escapeshellarg($cliDir));

        $ret = System::execute($cmd);

        if (strpos($ret, "Warning") !== FALSE) {
          $cmd = sprintf("%s update -n -d %s", $composer, escapeshellarg($cliDir));
          System::passthru($cmd);
        }

        clearstatcache();
      }

      if (file_exists($autoloadFile)) {
        require_once $autoloadFile;
      }
    }

    static function getComposerVer($ver) {
      if ($ver[0] == "^") $ver = substr($ver, 1);
      return $ver;
    }

    function generateComposer($vendorDir) {
      $repos = [];
      $composer = [];

      $list = $this->getComposerDependencies();
      foreach($this->cmds as $cmd) {
        $ret = $cmd->getComposerDependencies();
        if (!empty($ret)) {
          $list = array_merge($list, $ret);
        }
      }

      $list = array_unique($list);

      foreach($list as $key => $val) {
        if (isset($composer[$key])) {
          if (static::getComposerVer($val) <= static::getComposerVer($composer[$key])) {
            continue;
          }
        }

        if (strpos($key, "xano-marketplace/") === 0) {
          $repos[] = [
            "type" => "vcs",
            "url" => sprintf("https://%s:%s@gitlab.com/%s.git", "gitlab-token", "v9VfNzGt-SN_tW6Br1XG", $key)
          ];
        } elseif (strpos($val, "https://") === 0) {
          $parts = explode("#", $val);
          if (isset($parts[1])) {
            $url = $parts[0];
            $val = $parts[1];
          } else {
            $url = $val;
            $val = "dev-master";
          }

          $repos[] = [
            "type" => "package",
            "package" => [
              "name" => $key,
              "version" => $val,
              "type" => "package",
              "source" => [
                "type" => "git",
                "reference" => $val,
                "url" => $url
              ]
            ]
          ];
        }

        $composer[$key] = $val;
      }

      if (empty($composer)) return FALSE;

      ksort($composer);

      return [
        "config" => [
          "vendor-dir" => $vendorDir
        ],
        "require" => $composer,
        "repositories" => $repos,
      ];
    }

    function run() {
      $options = static::parseParams($this->args, $this->options);

      if (isset($options["u"])) {
        $_SERVER["XANO_USER"] = $options["u"];
      }

      try {
        if (!empty($this->action)) {
          $this->process($this->action, $this->args);
        } else {
          $this->printUsage();
        }
      } catch(\Exception $e) {
        printf("Error: %s\n", $e->getMessage());
      }

      exit(0);
    }

    function process($cmd, array $args = []) {
      if ( !($cmd instanceof \xano\cli\Command) ) {
        $cmd = $this->getCommand($cmd);
      }

      if (System::hasUser() && $cmd->getName() != "watchman-trigger") {
        $this->processComposer();
        try {
          $cfg = System::getConfig();
          if (isset($cfg["args"][$cmd->getName()])) {
            $args = array_merge($args, $cfg["args"][$cmd->getName()]);
          }
        } catch(\Exception $e) {
          // silently ignore
        }
      }

      $params = static::parseParams($args, $cmd->getOptions());

      $cmd->run($this, $params);
    }

    function printUsage() {
      printf("xano-cli %s\n\n", \xano\cli\Config::VERSION);
      printf("Usage: %s action [OPTIONS]\n\n", System::getBinaryName());

      printf("Global Options:\n");
      foreach($this->options as $opt) {
        printf("  %s: %s; %s\n", $opt->getMatch(), $opt->isRequired() ? "required" : "optional", $opt->getUsage());
      }

      printf("\nAvailable Actions:\n");
      $keys = array_keys($this->cmds);
      sort($keys);
      foreach($keys as $key) {
        $cmd = $this->cmds[$key];
        if (!$cmd->isHidden()) {
          printf("  %s - %s\n", $cmd->getName(), $cmd->getUsage());
        }
      }

      printf("\n");
    }

    static private function matches(\xano\cli\Option $opt, $arg) {
      $match = $opt->getMatch();
      if ($match == "*") return true;
      return $match === $arg;
    }

    static private function parseParams(array $args, array $opts) {
      $params = [];

      while(true) {
        $arg = array_shift($args);
        if (empty($arg)) break;

        foreach($opts as $opt) {
          $name = $opt->getName();
          if (static::matches($opt, $arg)) {
            if ($opt->getMatch() == "*") {
              array_unshift($args, $arg);
              $params[$name] = $args;
              break 2;
            }

            switch($opt->getType()) {
              case "text": {
                $params[$name] = array_shift($args);
              } break;
              case "bool": {
                $params[$name] = true;
              } break;
            }

            break;
          }
        }
      }

      foreach($opts as $opt) {
        if ($opt->isRequired() && !isset($params[$opt->getName()])) {
          throw new \Exception(sprintf("Required argument is missing: %s\n", $opt->getFullName()));
        }
      }

      return $params;
    }
  }